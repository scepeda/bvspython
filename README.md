### What is it?

App that does segmentation of the blood vessels in retinal images, using [DRIVE](http://www.isi.uu.nl/Research/Databases/DRIVE/download.php) (database of retinal fundus images). 

Based on Theano/Lasagne in Python

### Install? ###
Only Ubuntu for now :/

- Execute "cd bin"
- Execute "./install.sh" 

### How to run? ###
- Execute "./trainingBVS.sh" for training. 
- Execute "./validationBVS.sh" for validation. 

### Who do I talk to? ###

* Sebastian Cepeda [sebastian.cepeda.fuentealba@gmail.com](sebastian.cepeda.fuentealba@gmail.com)
