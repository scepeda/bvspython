### What is it?

Blood vessel segmentation in images, using [DRIVE](http://www.isi.uu.nl/Research/Databases/DRIVE/download.php) (database of retinal fundus images). 

Based on Theano/Lasagne in Python

### Dependencies? ###

sudo apt-get install python-pip python-dev gfortran libatlas-base-dev libfreetype6-dev python-opencv

### How to run? ###
- Execute "trainingBVS.sh" for training. 

### Who do I talk to? ###

* Sebastian Cepeda [sebastian.cepeda.fuentealba@outlook.com](sebastian.cepeda.fuentealba@outlook.com)

